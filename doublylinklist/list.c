/* Header files */ 
#include <stdio.h> 
#include <stdlib.h> 

/* Symbolic constants */ 
#define TRUE 1 
#define FALSE 0 
#define SUCCESS 1 
#define DATA_NOT_FOUND 2 
#define LIST_EMPTY 3

/* Type definitions */ 
struct node; 
typedef int data_t; 
typedef int ret_t; 
typedef int bool; 
typedef size_t len_t; 
typedef struct node node_t; 
typedef node_t list_t; 

/* Data layout definition */ 
struct node
{
	data_t data; 		/* Data field. In general, place satellite data pointer here */ 
	struct node *prev; 	/* Backward link */ 
	struct node *next; 	/* Forward link */ 
}; 

/* Interface function declarations: To be called by client */ 
/* List creation API */ 
list_t *create_list(void);

/* List data insertion API's */ 
ret_t insert_beg(list_t *p_lst, data_t new_data); 
ret_t insert_end(list_t *p_lst, data_t new_data); 
ret_t insert_after_data(list_t *p_lst, data_t e_data, data_t new_data); 
ret_t insert_before_data(list_t *p_lst, data_t e_data, data_t new_data); 

/* List data deletion API's */ 
ret_t delete_beg(list_t *p_lst); 
ret_t delete_end(list_t *p_lst); 
ret_t delete_data(list_t *p_lst, data_t d_data); 

/* List examination and deletion API's */ 
ret_t examine_beg(list_t *p_lst, data_t *p_beg_data); 
ret_t examine_end(list_t *p_lst, data_t *p_end_data); 
ret_t examine_and_delete_beg(list_t *p_lst, data_t *p_beg_data); 
ret_t examine_and_delete_end(list_t *p_lst, data_t *p_end_data); 

/* Miscallaneous list API's */ 
len_t length(list_t *p_lst); 
bool find(list_t *p_lst, data_t f_data); 
bool is_empty(list_t *p_lst); 
void display(list_t *p_lst); 

/* Inter-list API's */ 
list_t *concat(list_t *p_lst1, list_t *p_lst2); 
list_t *merge(list_t *p_lst1, list_t *p_lst2); 

/* Array to list and vice-versa conversion API's */ 
data_t *to_array(list_t *p_lst, len_t *p_array_len); 
list_t *to_list(data_t *p_array, len_t len); 

/* List destruction API's */ 
ret_t destroy(list_t **pp_lst); 

/* End of interface functions */ 

/* List auxillary functions: To be called by interface routines only */ 
static void g_insert(node_t *p_beg, node_t *p_mid, node_t *p_end); 
static void g_delete(node_t *p_node); 
static node_t *search_node(node_t *p_head_node, data_t search_data); 
static node_t *get_node(data_t new_data); 

/* Auxillary routine: To be called by list auxillary functions */ 
void *xcalloc(size_t n, size_t s); 

int main(void) 
{
	list_t *lst = NULL; 
	list_t *lst1 = NULL; 
	list_t *lst2 = NULL; 
	list_t *lst3 = NULL; 
	data_t data = 0; 
	data_t a[10] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100}; 
	ret_t ret = -1; 
	len_t len = -1; 
	bool b_ans = FALSE; 
	data_t *array = NULL; 
	len_t a_len = 0; 
	int i = -1; 
		
	lst = create_list(); 

	if(is_empty(lst) == TRUE)
		puts("is_empty:List is empty initially"); 
	else
	{
		puts("is_empty:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	if(delete_beg(lst) == LIST_EMPTY)
		puts("delete_beg:Cannot delete head of empty list"); 
	else
	{
		puts("delete_beg:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	if(delete_end(lst) == LIST_EMPTY)
		puts("delete_end:Cannot delete tail of empty list"); 
	else
	{
		puts("delete_end:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	if(examine_beg(lst, &data) == LIST_EMPTY)
		puts("examine_beg:Cannot examine head data of empty list"); 
	else
	{
		puts("examine_beg:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	if(examine_end(lst, &data) == LIST_EMPTY)
		puts("examine_end:Cannot examine tail of empty list"); 
	else
	{
		puts("examine_end:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	if(examine_and_delete_beg(lst, &data) == LIST_EMPTY)
		puts("examine_and_delete_beg:Cannot examine and delete head of empty list"); 
	else
	{
		puts("examine_and_delete_beg:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	if(examine_and_delete_end(lst, &data) == LIST_EMPTY) 
		puts("examine_and_delete_end:Cannot examine and delete tail of empty list"); 
	else
	{
		puts("examine_and_delete_end:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	len = length(lst); 
	printf("Initially:length(lst):%d\n", len); 

	for(data = 0; data < 5; data++)
	{
		ret = insert_beg(lst, data); 
		if(ret != SUCCESS)
		{
			puts("insert_beg:unexpected error"); 
			exit(EXIT_FAILURE); 
		}
	}
	printf("insert_beg:"); 
	display(lst); 

	for(data = 5; data < 10; data++)
	{
		ret = insert_end(lst, data); 
		if(ret != SUCCESS)
		{
			puts("insert_end:unexpected error"); 
			exit(EXIT_FAILURE); 
		}
	}
	printf("insert_end:"); 
	display(lst); 

	ret = insert_after_data(lst, 453, 100); 
	if(ret == DATA_NOT_FOUND)
		puts("insert_after_data:Cannot insert 100 after non-existent data 453"); 
	else
	{
		puts("insert_after_data:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	ret = insert_before_data(lst, 943, 200); 
	if(ret == DATA_NOT_FOUND)
		puts("insert_after_data:Cannot insert 200 before non-existent data 943"); 
	else
	{
		puts("insert_before_data:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	ret = insert_after_data(lst, 0, 100); 
	if(ret != SUCCESS)
	{
		puts("insert_after_data:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("insert_after_data:"); 
	display(lst); 

	ret = insert_before_data(lst, 0, 200); 
	if(ret != SUCCESS)
	{
		puts("insert_before_data:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("insert_before_data:"); 
	display(lst); 

	ret = delete_beg(lst); 
	if(ret != SUCCESS)
	{
		puts("delete_beg:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("delete_beg:"); 
	display(lst); 

	ret = delete_end(lst); 
	if(ret != SUCCESS)
	{
		puts("delete_end:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("delete_end:"); 
	display(lst); 

	ret = delete_data(lst, 0); 
	if(ret != SUCCESS)
	{
		puts("delete_data:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("delete_data:"); 
	display(lst); 

	data = -1; 
	ret = examine_beg(lst, &data); 
	if(ret != SUCCESS)
	{
		puts("examine_beg:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("Beginning data:%d\n", data); 

	data = -1; 
	ret = examine_end(lst, &data); 
	if(ret != SUCCESS)
	{
		puts("examine_end:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("End data:%d\n", data); 

	data = -1; 
	ret = examine_and_delete_beg(lst, &data); 
	if(ret != SUCCESS)
	{
		puts("examine_and_delete_beg:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("Examined and deleted begining data:%d\n", data); 
	printf("examine_and_delete_beg:"); 
	display(lst); 

	data = -1; 
	ret = examine_and_delete_end(lst, &data); 
	if(ret != SUCCESS)
	{
		puts("examine_and_delete_end:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("Examined and deleted end data:%d\n", data); 
	printf("examine_and_delete_end:"); 
	display(lst); 

	len = length(lst); 
	printf("Current:lenght(lst):%d\n", len); 

	b_ans = find(lst, -100); 
	if(b_ans == FALSE)
		puts("find:-100 is not present in list"); 
	else
	{
		puts("find:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	b_ans = find(lst, 7); 
	if(b_ans == TRUE)
		puts("find:7 is present in list"); 
	else
	{
		puts("find:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	b_ans = is_empty(lst); 
	if(b_ans == FALSE)
		puts("Current:is_empty:list is not empty"); 
	else
	{
		puts("is_empty:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	
	array = to_array(lst, &a_len); 
	if(array == NULL)
	{
		puts("to_array:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	puts("Displaying array:"); 
	printf("[beg]"); 
	for(i = 0; i < a_len; i++)
		printf("[%d]", array[i]); 
	printf("[end]\n"); 
	free(array); 
	array = NULL; 

	ret = destroy(&lst); 
	if(ret == SUCCESS && lst == NULL)
		puts("destroy:list is destroyed successfully"); 
	else
	{
		puts("destroy:unexpected error"); 
		exit(EXIT_FAILURE); 
	}

	lst = to_list(a, 10); 
	if(lst == NULL)
	{
		puts("to_list:unexpected error"); 
		exit(EXIT_FAILURE); 
	}
	printf("to_list:"); 
	display(lst); 
	ret = destroy(&lst); 
	if(ret == SUCCESS && lst == NULL)
		puts("destroy:lst destroyed successfully"); 

	lst1 = create_list(); 
	lst2 = create_list(); 
	for(data = 10; data < 100; data = data + 8)
		insert_end(lst1, data); 

	for(data = 20; data < 130; data = data + 15)
		insert_end(lst2, data); 

	printf("lst1:"); 
	display(lst1); 

	printf("lst2:"); 
	display(lst2); 

	lst3 = concat(lst1, lst2); 
	printf("concat:lst3:"); 
	display(lst3); 

	ret = destroy(&lst3); 
	if(ret == SUCCESS && lst3 == NULL)
		puts("destroy:lst3 destroyed successfully"); 

	lst3 = merge(lst1, lst2); 
	printf("merge:"); 
	display(lst3); 

	ret = destroy(&lst1); 
	if(ret == SUCCESS && lst1 == NULL)
		puts("destroy:lst1 destroyed successfully"); 

	ret = destroy(&lst2); 
	if(ret == SUCCESS && lst2 == NULL)
		puts("destroy:lst2 destroyed successfully"); 

	ret = destroy(&lst3); 
	if(ret == SUCCESS && lst3 == NULL)
		puts("destroy:lst3 is destroyed successfully"); 

	exit(EXIT_SUCCESS); 
}

/* Server side implementation */ 

/* Interface function declarations: To be called by client */ 
/* List creation API */ 
list_t *create_list(void)
{
	node_t *p_head_node = NULL; 

	p_head_node = get_node(0); 
	p_head_node->prev = p_head_node; 
	p_head_node->next = p_head_node; 
	return (p_head_node); 
}

/* List data insertion API's */ 
ret_t insert_beg(list_t *p_lst, data_t new_data)
{
	node_t *p_new_node = NULL; 

	p_new_node = get_node(new_data); 
	g_insert(p_lst, p_new_node, p_lst->next); 
	return (SUCCESS); 
}

ret_t insert_end(list_t *p_lst, data_t new_data)
{
	node_t *p_new_node = NULL; 

	p_new_node = get_node(new_data); 
	g_insert(p_lst->prev, p_new_node, p_lst); 
	return (SUCCESS); 
}

ret_t insert_after_data(list_t *p_lst, data_t e_data, data_t new_data)
{
	node_t *p_existing_node = NULL; 
	node_t *p_new_node = NULL; 

	p_existing_node = search_node(p_lst, e_data); 
	if(p_existing_node == NULL)
		return (DATA_NOT_FOUND); 
	
	p_new_node = get_node(new_data); 
	g_insert(p_existing_node, p_new_node, p_existing_node->next); 
	return (SUCCESS); 
}

ret_t insert_before_data(list_t *p_lst, data_t e_data, data_t new_data)
{
	node_t *p_existing_node = NULL; 
	node_t *p_new_node = NULL; 

	p_existing_node = search_node(p_lst, e_data); 
	if(p_existing_node == NULL)
		return (DATA_NOT_FOUND); 

	p_new_node = get_node(new_data); 
	g_insert(p_existing_node->prev, p_new_node, p_existing_node); 
	return (SUCCESS); 
}

/* List data deletion API's */ 
ret_t delete_beg(list_t *p_lst)
{
	if(is_empty(p_lst) == TRUE)
		return (LIST_EMPTY); 
	g_delete(p_lst->next); 
	return (SUCCESS); 
}

ret_t delete_end(list_t *p_lst)
{
	if(is_empty(p_lst) == TRUE)
		return (LIST_EMPTY); 
	g_delete(p_lst->prev); 
	return (SUCCESS); 
}

ret_t delete_data(list_t *p_lst, data_t d_data)
{
	node_t *p_delete_node = NULL; 

	p_delete_node = search_node(p_lst, d_data); 
	if(p_delete_node == NULL)
		return (DATA_NOT_FOUND); 
	g_delete(p_delete_node); 
	return (SUCCESS); 
}

/* List examination and deletion API's */ 
ret_t examine_beg(list_t *p_lst, data_t *p_beg_data)
{
	if(is_empty(p_lst) == TRUE)
		return (LIST_EMPTY); 
	*p_beg_data = p_lst->next->data; 
	return (SUCCESS); 
}

ret_t examine_end(list_t *p_lst, data_t *p_end_data)
{
	if(is_empty(p_lst) == TRUE)
		return (LIST_EMPTY); 
	*p_end_data = p_lst->prev->data; 
	return (SUCCESS); 
}

ret_t examine_and_delete_beg(list_t *p_lst, data_t *p_beg_data)
{
	if(is_empty(p_lst) == TRUE)
		return (LIST_EMPTY); 
	*p_beg_data = p_lst->next->data; 
	g_delete(p_lst->next); 
	return (SUCCESS); 
}

ret_t examine_and_delete_end(list_t *p_lst, data_t *p_end_data)
{
	if(is_empty(p_lst) == TRUE)
		return (LIST_EMPTY); 
	*p_end_data = p_lst->prev->data; 
	g_delete(p_lst->prev); 
	return (SUCCESS); 
}

/* Miscallaneous list API's */ 
len_t length(list_t *p_lst)
{
	len_t len = 0; 
	node_t *p_run = NULL; 

	p_run = p_lst->next; 
	while(p_run != p_lst)
	{
		len = len + 1; 
		p_run = p_run->next; 
	}

	return (len); 
}

bool find(list_t *p_lst, data_t f_data)
{
	return (search_node(p_lst, f_data) != NULL); 
}

bool is_empty(list_t *p_lst)
{
	return ((p_lst->prev == p_lst) && (p_lst->next == p_lst)); 
}

void display(list_t *p_lst)
{
	node_t *p_run = NULL; 

	printf("[beg]<->"); 
	p_run = p_lst->next; 
	while(p_run != p_lst)
	{
		printf("[%d]<->", p_run->data); 
		p_run = p_run->next; 
	}
	printf("[end]\n"); 
}

/* Inter-list API's */ 
list_t *concat(list_t *p_lst1, list_t *p_lst2)
{
	list_t *p_cat_lst = NULL; 
	node_t *p_run = NULL; 

	p_cat_lst = create_list(); 
	
	for(p_run = p_lst1->next; p_run != p_lst1; p_run = p_run->next)
		insert_end(p_cat_lst, p_run->data); 
	
	for(p_run = p_lst2->next; p_run != p_lst2; p_run = p_run->next)
		insert_end(p_cat_lst, p_run->data); 

	return (p_cat_lst); 
}

list_t *merge(list_t *p_lst1, list_t *p_lst2)
{
	list_t *p_merge_lst = NULL; 
	node_t *p_run1 = NULL; 
	node_t *p_run2 = NULL; 
	
	p_merge_lst = create_list(); 
	p_run1 = p_lst1->next; 
	p_run2 = p_lst2->next; 

	while(TRUE)
	{
		if(p_run1 == p_lst1)
		{
			while(p_run2 != p_lst2)
			{
				insert_end(p_merge_lst, p_run2->data); 
				p_run2 = p_run2->next; 
			}
			break; 
		}

		if(p_run2 == p_lst2)
		{
			while(p_run1 != p_lst1)
			{
				insert_end(p_merge_lst, p_run1->data); 
				p_run1 = p_run1->next; 
			}
			break; 
		}

		if(p_run1->data <= p_run2->data)
		{
			insert_end(p_merge_lst, p_run1->data); 
			p_run1 = p_run1->next; 
		}
		else
		{
			insert_end(p_merge_lst, p_run2->data); 
			p_run2 = p_run2->next; 
		}
	}

	return (p_merge_lst); 
}

/* Array to list and vice-versa conversion API's */ 
data_t *to_array(list_t *p_lst, len_t *p_array_len)
{
	len_t array_len = 0; 
	data_t *p_array = NULL; 
	node_t *p_run = NULL; 
	int i = -1; 

	array_len = length(p_lst); 
	*p_array_len = array_len; 
	p_array = (data_t*)xcalloc(array_len, sizeof(int)); 

	p_run = p_lst->next; 
	i = 0; 
	while(p_run != p_lst)
	{
		p_array[i] = p_run->data; 
		i = i + 1; 
		p_run = p_run->next; 
	}

	return (p_array); 

}

list_t *to_list(data_t *p_array, len_t len)
{
	int i = -1; 
	list_t *p_lst = NULL; 

	p_lst = create_list(); 
	for(i = 0; i < len; i++)
		insert_end(p_lst, p_array[i]); 

	return (p_lst); 
}

/* List destruction API's */ 
ret_t destroy(list_t **pp_lst) 
{
	node_t *p_run = NULL; 
	node_t *p_run_next = NULL; 
	node_t *p_head_node = NULL; 

	p_head_node = *pp_lst; 
	p_run = p_head_node->next; 
	while(p_run != p_head_node)
	{
		p_run_next = p_run->next; 
		free(p_run); 
		p_run = p_run_next; 
	}

	free(p_head_node);

	*pp_lst = NULL;
	return (SUCCESS); 
}

/* End of interface functions */ 

/* List auxillary functions: To be called by interface routines only */ 
static void g_insert(node_t *p_beg, node_t *p_mid, node_t *p_end)
{
	p_mid->next = p_end; 
	p_mid->prev = p_beg; 
	p_end->prev = p_mid; 
	p_beg->next = p_mid; 
}

static void g_delete(node_t *p_node)
{
	p_node->prev->next = p_node->next; 
	p_node->next->prev = p_node->prev; 
	free(p_node); 
	p_node = NULL; 
}

static node_t *search_node(node_t *p_head_node, data_t search_data)
{
	node_t *p_run = NULL; 

	p_run = p_head_node->next; 
	while(p_run != p_head_node)
	{
		if(p_run->data == search_data)
			return (p_run); 
		p_run = p_run->next; 
	}
	return ((node_t*)NULL); 
}

static node_t *get_node(data_t new_data)
{
	node_t *p_new_node = NULL; 

	p_new_node = (node_t*)xcalloc(1, sizeof(node_t)); 
	p_new_node->data = new_data; 
	return (p_new_node); 
}

/* Auxillary routine: To be called by list auxillary functions */ 
void *xcalloc(size_t n, size_t s) 
{
	void *p = NULL; 
	p = calloc(n, s); 
	if(p == NULL)
	{
		puts("xcalloc:fatal:out of memory"); 
		exit(EXIT_FAILURE); 
	}
	return (p); 
}

