.section .rodata
	msg_p1:
	.string "is_empty:List is empty initially"
	msg_err1:
	.string "is_empty:unexcepted error:"
	msg_p2:
	.string	"delete_beg:Cannot delete head of empty list"
	msg_err2:
	.string "delete_beg:unexcepted error:"
	msg_err3:
	.string "delete_end:unexcepted error:"
	msg_p3:
	.string	"delete_end: Cannot delete tail og empty list"
	msg_p4:
	.string "examine_beg:Cannot examine head data of empty list"
	msg_err4:
	.string	"examine_beg: unexpected error"
	msg_p5:
	.string	"examine_end:Cannot examine tail of empty list"
	msg_err5:
	.string	"examine_end: unexpected error"
	msg_p6:	
	.string	"examine_and_delete_beg:Cannot examine and delete head of empty list" 
	msg_err6:
	.string	"examine_and_delete_beg: unexpected error"
	msg_p7:	
	.string	"examine_and_delete_end:Cannot examine and delete tail of empty list" 
	msg_err7:
	.string	"examine_and_delete_end: unexpected error"
	msg_p8:
	.string	"Initially:length(lst):%d\n" 
	msg_err8:
	.string	"insert_beg:unexpected error" 
	msg_p9:
	.string	"insert_beg:" 
	msg_err9:
	.string	"insert_end:unexpected error" 
	msg_p10:
	.string	"insert_end:" 
	msg_p11:	
	.string	"insert_after_data:Cannot insert 100 after non-existent data 453"
	msg_err10:
	.string "insert_after_data:unexcepted error" 
	msg_p12:
	.string	"insert_after_data:Cannot insert 200 before non-existent data 943" 
	msg_err11:	
	.string	"insert_after_data:unexpected error"
	msg_p13:
	.string	"insert_after_data:"
	msg_err12:	
	.string	"insert_before_data:unexpected error"
	msg_p14:
	.string	"insert_before_data:"
	msg_err13:	
	.string	"delete_beg:unexpected error"
	msg_p15:
	.string	"delete_beg" 
	msg_err14:	
	.string	"delete_end:unexpected error"
	msg_p16:
	.string	"delete_end" 
	msg_err15:	
	.string	"delete_data:unexpected error" 
	msg_p17:
	.string	"delete_data:" 
	msg_err16:	
	.string	"examine_beg:unexpected error" 
	msg_p18:
	.string	"Beginning data:%d\n" 
	msg_err17:	
	.string	"examine_end:unexpected error"
	msg_p19:
	.string	"End data:%d\n" 
	msg_err18:	
	.string	"examine_and_delete_beg:unexpected error" 
	msg_p20:
	.string	"Examined and deleted begining data:%d\n"
	msg_p21: 
	.string	"examine_and_delete_beg:" 
	msg_err19:	
	.string	"examine_and_delete_end:unexpected error" 
	msg_p22:
	.string	"Examined and deleted end data:%d\n" 
	msg_p23:
	.string	"examine_and_delete_end:" 
	msg_p24:
	.string	"Current:lenght(lst):%d\n" 
	msg_p25:
	.string	"find:-100 is not present in list"
	msg_err20:
	.string	"find:unexpected error" 
	msg_p26:
	.string	"find:7 is not present in list"
	msg_p27:	
	.string	"Current:is_empty:list is not empty"
	msg_err21:
	.string	"is_empty:unexpected error" 
	msg_err22:
	.string	"to_array:unexpected error"
	msg_p28:
	.string	"Displaying array:"
	msg_p29: 
	.string	"[beg]"
	msg_p30:
	.string	"[%d]" 
	msg_p31:
	.string	"[end]\n" 
	msg_err23:
	.string	"destroy:unexpected error" 
	msg_p32:	
	.string	"destroy:list is destroyed successfully" 
	msg_err24:	
	.string	"to_list:unexpected error" 
	msg_p33:
	.string	"to_list:" 
	msg_p34:
	.string	"destroy:lst destroyed successfully" 
	msg_p35:
	.string	"lst1:"
	msg_p36:
	.string	"lst2:"
	msg_p37:
	.string	"concat:lst3:" 
	msg_p38:	
	.string	"destroy:lst3 destroyed successfully" 
	msg_p39:	
	.string	"merge:" 
	msg_p40:
	.string	"destroy:lst1 destroyed successfully"
	msg_p41:
	.string	"destroy:lst2 destroyed successfully"
	msg_p42:
	.string	"destroy:lst3 destroyed successfully"
	msg_p43:
	.string	"[beg]<->"
	msg_p44:
	.string	"[%d]<->"
	msg_p45:
	.string	"[end]\n"
	msg_p46:	
	.string	"xcalloc:fatal:out of memory" 
	 
	 
.section .text
	 .globl main
	 .type  main,@function
main:
	pushl 	%ebp
	movl	%esp,	%ebp
	andl	$-16,	%esp
	subl	$96,	%esp
	###########first  node#############
	movl	$0,	-4(%ebp)#####lst###
	movl	$0,	-8(%ebp)####lst1###
	movl	$0,	-12(%ebp)####lst2####
	movl	$0,	-16(%ebp)####lst3####
	############variable data###############
	movl	$0,	-20(%ebp)####data#####
	############data array#################
	movl	$10,	-24(%ebp)#####a[10]####	
	movl	$20,	-28(%ebp)	
	movl	$30,	-32(%ebp)	
	movl	$40,	-36(%ebp)	
	movl	$50,	-40(%ebp)	
	movl	$60,	-44(%ebp)	
	movl	$70,	-48(%ebp)	
	movl	$80,	-52(%ebp)	
	movl	$90,	-56(%ebp)	
	movl	$100,	-60(%ebp)
	#############int varables##############
	movl	$-1,	-64(%ebp)####ret#####
	movl	$-1,	-68(%ebp)####len#####
	movl	$0,	-72(%ebp)####b_ans#####
	movl	$0,	-76(%ebp)####*array####
	movl	$0,	-80(%ebp)####a_len####
	movl	$-1,	-84(%ebp)#####i######
	#############code start###############
	call	create_list
	movl	%eax,	-4(%ebp)
	movl	%eax,	(%esp)
	call	is_empty	
	cmpl	$1,	%eax
	jne	else1
	movl	$msg_p1,(%esp)
	call	puts	
	jmp	contu
else1:	
	movl	$msg_err1,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
contu:
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	delete_beg
	cmpl	$3,	%eax
	jne	else2
	movl	$msg_p2,(%esp)
	call	puts
	jmp 	cont1
else2:
	movl	$msg_err2,(%esp)
	call	puts			
	movl	$-1,	(%esp)
	call	exit
cont1:
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	delete_end
	cmpl	$3,	%eax
	jne	else3
	movl	$msg_p3,(%esp)
	call	puts
	jmp 	cont2
else3:
	movl	$msg_err3,(%esp)
	call	puts			
	movl	$-1,	(%esp)
	call	exit
cont2:
	leal	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	examine_beg
	cmpl	$3,	%eax
	jne	else4
	movl	$msg_p4,(%esp)
	call	puts
	jmp	cont3	
else4:
	movl	$msg_err4,(%esp)
	call	puts			
	movl	$-1,	(%esp)
	call	exit

cont3:
	leal	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	examine_end
	cmpl	$3,	%eax
	jne	else5
	movl	$msg_p5,(%esp)
	call	puts
	jmp	cont4	
else5:
	movl	$msg_err5,(%esp)
	call	puts			
	movl	$-1,	(%esp)
	call	exit
cont4:
	leal	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	examine_and_delete_beg
	cmpl	$3,	%eax
	jne	else6
	movl	$msg_p6,(%esp)
	call	puts
	jmp	cont5	
else6:
	movl	$msg_err6,(%esp)
	call	puts			
	movl	$-1,	(%esp)
	call	exit
cont5:
	leal	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	examine_and_delete_end
	cmpl	$3,	%eax
	jne	else7
	movl	$msg_p7,(%esp)
	call	puts
	jmp	cont6	
else7:
	movl	$msg_err7,(%esp)
	call	puts			
	movl	$-1,	(%esp)
	call	exit

cont6:
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	length
	movl	%eax,	-68(%ebp)
	movl	-68(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	$msg_p8,(%esp)
	call	printf	
	
	movl	$0,	-20(%ebp)
	jmp 	mn_cond
mn_for:
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	movl	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	call	insert_beg
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)		
	jne	out
	jmp	add
out:
	movl	$msg_err8,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
add:
	addl	$1,	-20(%ebp)
mn_cond:
	movl	-20(%ebp),%eax
	cmpl	$5,	%eax
	jl	mn_for
	
	movl	$msg_p9,(%esp)
	call	puts

	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	display			
				
	movl	$5,	-20(%ebp)
	jmp 	mn_cond1
mn_for1:
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	movl	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	call	insert_end
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)		
	jne	out1
	jmp	add1
out1:
	movl	$msg_err9,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
add1:
	addl	$1,	-20(%ebp)
mn_cond1:
	movl	-20(%ebp),%eax
	cmpl	$10,	%eax
	jl	mn_for1
	
	movl	$msg_p10,(%esp)
	call	puts
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	display
	
	movl	$100,	8(%esp)
	movl	$453,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	insert_after_data
	movl	%eax,	-64(%ebp)
	cmpl	$2,	-64(%ebp)
	jne	else8
	movl	$msg_p11,(%esp)
	call	puts
	jmp 	cont7
else8:	
	movl	$msg_err10,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit

cont7:
	movl	$200,	8(%esp)
	movl	$943,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	insert_before_data
	movl	%eax,	-64(%ebp)
	cmpl	$2,	-64(%ebp)
	jne	else9
	movl	$msg_p12,(%esp)
	call	puts
	jmp 	cont8
else9:	
	movl	$msg_err10,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit

cont8:
	movl	$100,	8(%esp)
	movl	$0,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	insert_after_data
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	out2
	jmp	cont9
out2:
	movl	$msg_err11,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
cont9:
	movl	$msg_p13,(%esp)
	call	puts
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	display	
	
	movl	$200,	8(%esp)
	movl	$0,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	insert_before_data
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	out3
	jmp	contt9
out3:
	movl	$msg_err11,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
contt9:
	movl	$msg_p14,(%esp)
	call	puts
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	display

	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	delete_beg
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	out4
	jmp	cont10
out4:
	movl	$msg_err13,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
cont10:
	movl	$msg_p15,(%esp)
	call	puts
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	display
	
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	delete_end
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	out5
	jmp	contt11
out5:
	
	movl	$msg_err14,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
contt11:
	movl	$msg_p16,(%esp)
	call	puts
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	display
	
	movl	$0,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	delete_data
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	out6
	jmp	cont11
out6:
	movl	$msg_err15,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
cont11:
	movl	$msg_p17,(%esp)
	call	puts
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	display
	
	movl	$-1,	-20(%ebp)
	leal	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	examine_beg	
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	out7
	jmp	cont12
out7:			
	movl	$msg_err16,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit

cont12:
	movl	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	$msg_p18,(%esp)
	call	printf
	
	movl	$-1,	-20(%ebp)
	leal	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	examine_end	
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	out8
	jmp	cont13
out8:
	movl	$msg_err17,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit

cont13:
	movl	$msg_p19,(%esp)
	movl	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	call	printf
	
	movl	$-1,	-20(%ebp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	leal	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	call	examine_and_delete_beg
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	out9
	jmp	cont14
out9:
	movl	$msg_err18,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
cont14:
	movl	-20(%ebp),%eax
	movl	%eax,	4(%esp)		
	movl	$msg_p20,(%esp)
	call	printf
	movl	$msg_p21,(%esp)
	call	printf
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	display
	
	movl	$-1,	-20(%ebp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	leal	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	call	examine_and_delete_end
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	out10
	jmp	cont15
out10:
	movl	$msg_err19,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
cont15:
	movl	-20(%ebp),%eax
	movl	%eax,	4(%esp)		
	movl	$msg_p22,(%esp)
	call	printf
	movl	$msg_p23,(%esp)
	call	printf
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	display
	
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	length
	movl	%eax,	-68(%ebp)
	movl	-68(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	$msg_p24,(%esp)
	call	printf	
	
	movl	$-100,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	find	
	movl	%eax,	-72(%ebp)
	cmpl	$0,	-72(%ebp)
	jne	else10
	movl	$msg_p25,(%esp)
	call	puts
	jmp	cont16
else10:
	movl	$msg_err20,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
cont16:
	movl	$7,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	find	
	movl	%eax,	-72(%ebp)
	cmpl	$1,	-72(%ebp)
	jne	elsee11
	movl	$msg_p26,(%esp)
	call	puts
	jmp	cont17
elsee11:
	movl	$msg_err20,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
cont17:
	movl	-4(%ebp),%eax
	movl	%eax,	4(%esp)
	call	is_empty
	movl	%eax,	-72(%ebp)
	cmpl	$0,	-72(%ebp)
	jne	else11
	movl	$msg_p27,(%esp)
	call	puts
	jmp	cont18
else11:
	movl	$msg_err21,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
cont18:
	leal	-80(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	to_array
	movl	%eax,	-76(%ebp)
	cmpl	$0,	-76(%ebp)
	je	out11
	jmp	cont19
out11:
	movl	$msg_err22,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit

cont19:
	movl	$msg_p28,(%esp)
	call	puts
	movl	$msg_p29,(%esp)
	call	puts
	movl	$0,	-84(%ebp)
	jmp	cond2
for2:
	movl	-84(%ebp),%eax
	movl	-76(%ebp),%ebx
	leal	0(,%eax,4),%edx
	addl	%ebx,	%edx
	movl	(%edx),	%edx
	movl	%edx,	4(%esp)
	movl	$msg_p30,(%esp)
	call	printf	
	addl	$1,	-84(%ebp)
cond2:
	movl	-80(%ebp),%eax
	movl	-84(%ebp),%ebx
	cmpl	%eax,	%ebx
	jb	for2		
	
	movl	$msg_p31,(%esp)
	call	printf							

	movl	-76(%ebp),%eax
	movl	%eax,	(%esp)
	call	free
	movl	$0,	-76(%ebp)
	
	leal	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	destroy
	movl	%eax,	-64(%ebp)

	cmpl	$1,	-64(%ebp)
	jne	out12
	cmpl	$0,	-4(%ebp)
	jne	out12
	movl	$msg_p32,(%esp)
	call	puts
	jmp	cont20
out12:
	movl	$msg_err23,(%esp)
	call	puts	
	movl	$-1,	(%esp)
	call	exit

cont20:
	movl	-24(%ebp),%eax
	movl	%eax,	(%esp)
	movl	$10,	4(%esp)
	call	to_list
	movl	%eax,	-4(%ebp)
	
	cmpl	$0,	-4(%ebp)
	jne	cont21
	movl	$msg_err24,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
cont21:					
	movl	$msg_p33,(%esp)
	call	printf	
	
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call 	display	
	leal	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	destroy		
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	cont22
	cmpl	$0,	-4(%ebp)
	jne	cont22
	movl	$msg_p34,(%esp)
	call	puts
cont22:
	call	create_list
	movl	%eax,	-8(%ebp)
	call	create_list
	movl	%eax,	-12(%ebp)

	movl	$10,	-20(%ebp)
	jmp	cond3
for3:
	movl	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-8(%ebp),%eax
	movl	%eax,	(%esp)
	call	insert_end
	addl	$8,	-20(%ebp)
cond3:
	cmpl	$100,	-20(%ebp)
	jl	for3			
	
	movl	$20,	-20(%ebp)
	jmp	cond4
for4:
	movl	-20(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-12(%ebp),%eax
	movl	%eax,	(%esp)
	call	insert_end
	addl	$15,	-20(%ebp)
cond4:
	cmpl	$130,	-20(%ebp)
	jl	for4			

	movl	$msg_p35,(%esp)
	call	printf
	movl	-8(%ebp),%eax
	movl	%eax,	(%esp)
	call	display

	movl	$msg_p36,(%esp)
	call	printf
	movl	-12(%ebp),%eax
	movl	%eax,	(%esp)
	call	display
	
	movl	-8(%ebp),%eax
	movl	%eax,	(%esp)
	movl	-12(%ebp),%eax
	movl	%eax,	4(%esp)
	call	concat
	movl	%eax,	-16(%ebp)
	movl	$msg_p37,(%esp)
	call	puts
	movl	-16(%ebp),%eax
	movl	%eax,	(%esp)
	call	display
	
	leal	-16(%ebp),%eax
	movl	%eax,	(%esp)
	call	destroy
	
	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	cont23
	cmpl	$0,	-16(%ebp)
	jne	cont23
	movl	$msg_p38,(%esp)
	call	puts
cont23:
	movl	-8(%ebp),%eax
	movl	%eax,	(%esp)
	movl	-12(%ebp),%eax
	movl	%eax,	4(%esp)
	call	merge
	movl	%eax,	-16(%ebp)
	movl	$msg_p39,(%esp)
	call	puts
	movl	-16(%ebp),%eax	
	movl	%eax,	(%esp)
	call	display
	
	leal	-8(%ebp),%eax
	movl	%eax,	(%esp)
	call	destroy

	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	cont24
	cmpl	$0,	-8(%ebp)
	jne	cont24
	movl	$msg_p40,(%esp)
	call	puts
cont24:
	leal	-12(%ebp),%eax
	movl	%eax,	(%esp)
	call	destroy

	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	cont25
	cmpl	$0,	-12(%ebp)
	jne	cont25
	movl	$msg_p41,(%esp)
	call	puts
cont25:
	leal	-16(%ebp),%eax
	movl	%eax,	(%esp)
	call	destroy

	movl	%eax,	-64(%ebp)
	cmpl	$1,	-64(%ebp)
	jne	cont26
	cmpl	$0,	-16(%ebp)
	jne	cont26
	movl	$msg_p42,(%esp)
	call	puts

cont26:
	movl	$0,	(%esp)
	call	exit

.globl	create_list
.type	create_list,@function
create_list:
	pushl	%ebp
	movl	%esp,	%ebp

	subl	$16,	%esp
	movl	$0,	(%esp)
	call	get_node

	movl	%eax, 4(%eax)
	movl	%eax, 8(%eax) 

	movl	%ebp,	%esp
	popl	%ebp	
	ret

.globl	insert_beg
.type	insert_beg,@function
insert_beg:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	movl	$0,	-4(%ebp)
	
	movl	12(%ebp),%eax
	movl	%eax,	(%esp)
	call	get_node
	movl	%eax,	-4(%ebp)
	movl	8(%ebp),%ebx
	movl	%ebx,	(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	8(%esp)
	call	g_insert		
	movl	$1,	%eax
	movl	%ebp,	%esp
	popl	%ebp	
	ret

.globl	insert_end
.type	insert_end,@function
insert_end:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp

	movl	$0,	-4(%ebp)
	movl	12(%ebp),%eax
	movl	%eax,	(%esp)	
	call	get_node
	movl	%eax,	-4(%ebp)
	movl	8(%ebp),%ebx
	movl	4(%ebx),%ebx
	movl	%ebx,	(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	4(%esp)
	movl 	8(%ebp), %ebx
	movl	%ebx,	8(%esp)
	call	g_insert
	
	movl	$1,	%eax
	movl	%ebp,	%esp
	popl	%ebp	
	ret

.globl	insert_after_data
.type	insert_after_data,@function
insert_after_data:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$32,	%esp

	movl	$0,	-4(%ebp)
	movl	$0,	-8(%ebp)
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	movl	12(%ebp),%eax
	movl	%eax,	4(%esp)
	call	search_node
	movl	%eax,	-4(%ebp)
	cmpl	$0,	-4(%ebp)
	jne	ins_cont
	movl	$2,	%eax	
	jmp	ad_out
ins_cont:
	movl	16(%ebp),%eax
	movl	%eax,	(%esp)
	call	get_node
	movl	%eax,	-8(%ebp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	movl	-8(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-4(%ebp),%ebx
	movl	8(%ebx),%ebx
	movl	%ebx,	8(%esp)	
	call	g_insert		

	movl	$1,	%eax
ad_out:
	movl	%ebp,	%esp
	popl	%ebp	
	ret	

.globl	insert_before_data
.type	insert_before_data,@function
insert_before_data:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$32,	%esp

	movl	$0,	-4(%ebp)
	movl	$0,	-8(%ebp)
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	movl	12(%ebp),%eax
	movl	%eax,	4(%esp)
	call	search_node
	movl	%eax,	-4(%ebp)
	cmpl	$0,	-4(%ebp)
	jne	ins_cont2
	movl	$2,	%eax	
	jmp	ad_out2
ins_cont2:
	movl	16(%ebp),%eax
	movl	%eax,	(%esp)
	call	get_node
	movl	%eax,	-8(%ebp)
	movl	-4(%ebp),%ebx
	movl	4(%ebx),%ebx
	movl	%ebx,	(%esp)	
	movl	-8(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	8(%esp)
	call	g_insert		

	movl	$1,	%eax
ad_out2:
	movl	%ebp,	%esp
	popl	%ebp	
	ret	

.globl	delete_beg
.type	delete_beg,@function
delete_beg:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp

	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	call	is_empty
	cmpl	$1,	%eax
	jne	del_cont
	movl	$3,	%eax
	jmp	tp1
del_cont:	
	movl	8(%ebp),%ebx
	movl	8(%ebx),%ebx
	movl	%ebx,	(%esp)
	call	g_delete

	movl	$1,	%eax
tp1:
	movl	%ebp,	%esp
	popl	%ebp
	ret	

.globl	delete_end
.type	delete_end,@function
delete_end:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp

	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	call	is_empty
	cmpl	$1,	%eax
	jne	del_cont2
	movl	$3,	%eax
	jmp	tp2
del_cont2:	
	movl	8(%ebp),%ebx
	movl	4(%ebx),%ebx
	movl	%ebx,	(%esp)
	call	g_delete

	movl	$1,	%eax
tp2:
	movl	%ebp,	%esp
	popl	%ebp
	ret	

.globl	delete_data
.type	delete_data,@function
delete_data:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	$0,	-4(%ebp)
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	movl	12(%ebp),%eax
	movl	%eax,	4(%esp)
	call	search_node
	movl	%eax,	-4(%ebp)
	cmpl	$0,	-4(%ebp)
	jne	dd_cont
	movl	$2,	%eax
	jmp 	ddata
dd_cont:
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)		
	call	g_delete

	movl	$1,	%eax
ddata:
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	examine_beg
.type	examine_beg,@function
examine_beg:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	call	is_empty
	cmpl	$1,	%eax
	jne	exam_bcont
	movl	$3,	%eax
	jmp	tp3
exam_bcont:
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	(%ebx),	%ebx
	movl	12(%ebp),%eax
	movl	%ebx,(%eax)
	

	movl	$1,	%eax
tp3:
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	examine_end
.type	examine_end,@function
examine_end:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	call	is_empty
	cmpl	$1,	%eax
	jne	exam_econt
	movl	$3,	%eax
	jmp	tp4
exam_econt:
	movl	8(%ebp),%eax
	movl	4(%eax),%ebx
	movl	(%ebx),	%ebx
	movl	12(%ebp),%eax
	movl	%ebx,(%eax)
	

	movl	$1,	%eax
tp4:	
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	examine_and_delete_beg
.type	examine_and_delete_beg,@function
examine_and_delete_beg:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	call	is_empty
	cmpl	$1,	%eax
	jne	exam_adcont
	movl	$3,	%eax
	jmp	tp5
exam_adcont:
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	(%ebx),	%ebx
	movl	12(%ebp),%eax
	movl	%ebx,(%eax)
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	(%esp)
	call	g_delete

	movl	$1,	%eax
tp5:
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	examine_and_delete_end
.type	examine_and_delete_end,@function
examine_and_delete_end:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	call	is_empty
	cmpl	$1,	%eax
	jne	exam_adecont
	movl	$3,	%eax
	jmp	tp6
exam_adecont:
	movl	8(%ebp),%eax
	movl	4(%eax),%ebx
	movl	(%ebx),	%ebx
	movl	12(%ebp),%eax
	movl	%ebx,(%eax)
	movl	8(%ebp),%eax
	movl	4(%eax),%ebx
	movl	%ebx,	(%esp)
	call	g_delete

	movl	$1,	%eax
tp6:	
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	length
.type	length,@function
length:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	$0,	-4(%ebp)
	movl	$0,	-8(%ebp)
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	-8(%ebp)
	jmp	len_cond
len_while:
	addl	$1,	-4(%ebp)
	movl	-8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	-8(%ebp)
len_cond:
	movl	8(%ebp),%eax
	cmpl	%eax,	-8(%ebp)
	jne	len_while
	
	movl	-4(%ebp),%eax
	movl	%ebp,	%esp
	popl	%ebp
	ret
	
.globl	find
.type	find,@function
find:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	movl	12(%ebp),%eax
	movl	%eax,	4(%esp)
	call	search_node
	cmpl	$0,	%eax
	je	find_out
	movl	$1,	%eax
	jmp 	fnd_out
find_out:
	movl	$0,	%eax
fnd_out:
	movl	%ebp,	%esp
	popl	%ebp
	ret
	
.globl	is_empty
.type	is_empty,@function
is_empty:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	8(%ebp),%eax
	movl	4(%eax),%ebx
	
	cmpl	8(%ebp),%ebx
	jne	emp_out
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	cmpl	8(%ebp),%ebx
	jne	emp_out
	movl	$1,	%eax
	jmp	emp_suc
emp_out:
	movl	$0,	%eax
emp_suc:	
	movl	%ebp,	%esp
	popl	%ebp
	ret
.globl display
.type  display,@function		
display:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	$0,	-4(%ebp)
	movl	$msg_p43,(%esp)
	call	printf
	movl	8(%ebp),%ebx
	movl	8(%ebx),%ebx
	movl	%ebx,	-4(%ebp)
	jmp	dis_cond
dis_while:
	movl	$msg_p44,(%esp)
	movl	-4(%ebp),%eax
	movl	(%eax),	%eax
	movl	%eax,	4(%esp)
	call	printf
	movl	-4(%ebp),%ebx
	movl	8(%ebx),%eax
	movl	%eax,	-4(%ebp)
dis_cond:
	movl	8(%ebp),%eax
	cmpl	%eax,	-4(%ebp)
	jne	dis_while
		
	movl	$msg_p45,(%esp)
	call	printf
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	concat
.type	concat,@function
concat:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	$0,	-4(%ebp)
	movl	$0,	-8(%ebp)
	call	create_list
	movl	%eax,	-4(%ebp)
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	-8(%ebp)
	jmp	concat_cond
concat_for:
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	movl	-8(%ebp),%eax
	movl	(%eax),	%eax
	movl	%eax,	4(%esp)
	call	insert_end
	movl	-8(%ebp),%eax
	movl	8(%eax), %ebx
	movl	%ebx,	-8(%ebp)
concat_cond:
	movl	8(%ebp),%eax
	cmpl	%eax,	-8(%ebp)
	jne	concat_for

	movl	12(%ebp),%eax
	movl	8(%eax), %ebx
	movl	%ebx,	-8(%ebp)
	jmp	concat_cond1
concat_for1:
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	movl	-8(%ebp),%eax
	movl	(%eax),	%eax
	movl	%eax,	4(%esp)
	call	insert_end
	movl	-8(%ebp),%eax
	movl	8(%eax), %ebx
	movl	%ebx,	-8(%ebp)
concat_cond1:
	movl	12(%ebp),%eax
	cmpl	%eax,	-8(%ebp)
	jne	concat_for1
	

	movl	-4(%ebp),%eax
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	merge
.type	merge,@function
merge:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$32,	%esp
	
	movl	$0,	-4(%ebp)
	movl	$0,	-8(%ebp)
	movl	$0,	-12(%ebp)
	call	create_list
	movl	%eax,	-4(%ebp)
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	-8(%ebp)
	movl	12(%ebp),%eax
	movl	8(%eax), %ebx
	movl	%ebx,	-12(%ebp)
	#movl	$1,	%eax
	#jmp	mer_cond
mer_while:
	movl	8(%ebp),%ebx
	cmpl	%ebx,	-8(%ebp)
	jne	mer_nif
	jmp	mer_cond1
mer_while1:
	movl	-12(%ebp),%ebx
	movl	(%ebx), %ebx
	movl	%ebx,	4(%esp)
	movl	-4(%ebp),%ebx
	movl	%ebx,	(%esp)
	call	insert_end
	movl	-12(%ebp),%ebx
	movl	8(%ebx), %ecx
	movl	%ecx,	-12(%ebp)
mer_cond1:
	movl	12(%ebp),%ebx
	cmpl	%ebx,	-12(%ebp)
	jne	mer_while1			
	jmp 	mer_exit
mer_nif:
	movl	12(%ebp),%ebx
	cmpl	%ebx,	-12(%ebp)
	jne	mer_nif2
	jmp	mer_cond2
mer_while2:
	movl	-4(%ebp),%ebx
	#movl	(%ebx),	%ebx
	movl	%ebx,	(%esp)
	movl	-8(%ebp),%ebx
	movl	(%ebx),	%ebx
	movl	%ebx,	4(%esp)
	call	insert_end
	movl	-8(%ebp),%ebx
	movl	8(%ebx), %ebx
	movl	%ebx,	-8(%ebp)	
mer_cond2:
	movl	8(%ebp),%ebx
	cmpl	%ebx,	-8(%ebp)
	jne	mer_while2
	jmp 	mer_exit
mer_nif2:	
	movl	-8(%ebp),%ebx
	movl	(%ebx),	%ebx
	movl	-12(%ebp),%ecx
	movl	(%ecx),	%ecx
	cmpl	%ecx,	%ebx
	jg	mer_else
	movl	-4(%ebp),%ebx
	#movl	(%ebx),	%ebx
	movl	%ebx,	(%esp)
	movl	-8(%ebp),%ebx
	movl	(%ebx),	%ebx
	movl	%ebx,	4(%esp)
	call	insert_end
	movl	-8(%ebp),%ebx
	movl	8(%ebx),%ecx
	movl	%ecx,	-8(%ebp)
	jmp	mer_while
mer_else:
	movl	-4(%ebp),%ebx
	#movl	(%ebx),	%ebx
	movl	%ebx,	(%esp)
	movl	-12(%ebp),%ebx
	movl	(%ebx),	%ebx
	movl	%ebx,	4(%esp)
	call	insert_end
	movl	-12(%ebp),%ebx
	movl	8(%ebx), %ecx
	movl	%ecx,	-12(%ebp)
	jmp	mer_while
mer_exit:	
	movl	-4(%ebp),%eax
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	to_array
.type	to_array,@function
to_array:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$32,	%esp
	
	movl	$0,	-4(%ebp)
	movl	$0,	-8(%ebp)
	movl	$0,	-12(%ebp)
	movl	$-1,	-16(%ebp)
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	call	length
	movl	%eax,	-4(%ebp)
	movl	12(%ebp),%ebx
	movl	%eax,	(%ebx)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)	
	movl	$4,	4(%esp)
	call	xcalloc
	movl	%eax,	-8(%ebp)
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	-12(%ebp)
	movl	$0,	-16(%ebp)
	jmp	to_cond
to_while:
	movl	-16(%ebp),%eax
	movl	-8(%ebp),%ebx
	leal	(%ebx,%eax,4),%ebx
	movl	-12(%ebp),%eax
	movl	(%eax),	%eax
	movl	%eax,	(%ebx)
	addl	$1,	-16(%ebp)
	movl	-12(%ebp),%ebx
	movl	8(%ebx),%eax
	movl	%eax,	-12(%ebp)
to_cond:
	movl	8(%ebp),%eax
	cmpl	%eax,	-12(%ebp)
	jne	to_while

	movl	-8(%ebp),%eax
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	to_list
.type	to_list,@function
to_list:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	$-1,	-4(%ebp)
	movl	$0,	-8(%ebp)
	call	create_list
	movl	%eax , -8(%ebp)
	movl	$0,	-4(%ebp)
	jmp	list_cond
list_for:
	movl	-8(%ebp),%eax
	movl	%eax,	(%esp)
	movl	-4(%ebp),%eax
	movl	8(%ebp),%ebx
	leal	(%ebx,%eax,4),%ebx
	#movl	(%ebx),	%ebx
	movl	%ebx,	4(%esp)
	call	insert_end
	addl	$1,	-4(%ebp)
list_cond:
	movl	12(%ebp),%eax
	cmpl	%eax,	-4(%ebp)
	jl	list_for

	movl	-8(%ebp),%eax
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	destroy
.type	destory,@function
destroy:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$32,	%esp
	
	movl	$0,	-4(%ebp)
	movl	$0,	-8(%ebp)
	movl	$0,	-12(%ebp)
	movl	8(%ebp),%eax
	movl	(%eax),	%eax
	movl	%eax,	-12(%ebp)
	movl	-12(%ebp),%ebx
	movl	8(%ebx),%ebx
	movl	%ebx,	-4(%ebp)
	jmp	dest_cond
dest_while:
	movl	-4(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	-8(%ebp)
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	free
	movl	-8(%ebp),%ebx
	movl	%ebx,	-4(%ebp)
dest_cond:
	movl	-12(%ebp),%eax
	cmpl	%eax,	-4(%ebp)
	jne	dest_while	
	movl	-12(%ebp),%eax
	movl	%eax,	(%esp)
	call	free
	movl	8(%ebp),%eax
	movl	$0,	(%eax)
	
	movl	$1,	%eax
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	g_insert
.type	g_insert,@function
g_insert:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	8(%ebp), %eax
	movl	12(%ebp), %ebx
	movl	16(%ebp), %edx
	
	movl	%eax, 4(%ebx)
	movl	%edx, 8(%ebx) 
	movl	%ebx, 4(%edx)
	movl	%ebx, 8(%eax) 

	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	g_delete
.type	g_delete,@function
g_delete:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	8(%ebp),%eax
	movl	4(%eax),%eax
	movl	8(%ebp),%ebx
	movl	8(%ebx),%ebx
	movl	%ebx,	8(%eax)
	
	movl	8(%ebp),%eax
	movl	4(%eax),%ebx
	movl	8(%ebp),%eax
	movl	8(%eax),%ecx
	movl	%ebx,	4(%ecx)
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	call	free
	movl	$0,	8(%ebp)

	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	search_node
.type	search_node,@function
search_node:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp

	movl	$0,	-4(%ebp)
	movl	8(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	-4(%ebp)
	jmp	search_cond
search_while:
	movl	-4(%ebp),%eax
	movl	(%eax),	%eax
	cmpl	12(%ebp),%eax
	jne	search_out
	movl	-4(%ebp),%eax
	jmp	ser_out
search_out:
	movl	-4(%ebp),%eax
	movl	8(%eax),%ebx
	movl	%ebx,	-4(%ebp)
search_cond:
	movl	8(%ebp),%eax
	cmpl	%eax,	-4(%ebp)
	jne	search_while

	movl	$0,	%eax
ser_out:
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	get_node
.type	get_node,@function
get_node:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp

	movl	$0,	-4(%ebp)
	movl	$1,	(%esp)
	movl	$12,	4(%esp)
	call	xcalloc
	
	movl	8(%ebp), %edx
	movl	%edx, (%eax) 

	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	xcalloc
.type	xcalloc,@function
xcalloc:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp

	movl	$0,	-4(%ebp)	
	movl	8(%ebp),%eax
	movl	%eax,	(%esp)
	movl	12(%ebp),%ebx
	movl	%ebx,	4(%esp)
	call	calloc
	movl	%eax,	-4(%ebp)
	cmpl	$0,	%eax
	jne	xcalloc_out
	movl	$msg_p46,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
xcalloc_out:
	movl	-4(%ebp),%eax
	movl	%ebp,	%esp
	popl	%ebp
	ret
