.section .rodata
	msg:
	.string "%d\n"
	msg_p1_err:
	.string "Usage error"
	msg_p2_err:
	.string "invalid command line argument"
	msg_p3:
	.string "sort success"
	msg_p4_err:
	.string "sort failure"
	msg_p5:
	.string "a[%d]:%d\n"
	msg_p6_err:
	.string "fatal memory"	

.section .text
	.globl main
	.type main,@function

main:
	###############################################
	pushl	%ebp
	movl	%esp,	%ebp
	andl	$-16,	%esp
	subl	$16,	%esp
	###############################################
	movl	4(%ebp),%eax
	movl	$0,	-4(%ebp)
	movl	$0,	-8(%ebp)
	cmpl	$2,	 %eax
	jne	usage
	movl	12(%ebp), %eax
	movl	%eax,	(%esp)
	call	atoi
	movl	%eax,	-8(%ebp)
	cmpl	$0,	-8(%ebp)
	jle	invalid
	
	movl	-8(%ebp),%eax
	movl	%eax,	(%esp)
	movl	$4,	4(%esp)
	call	xcalloc
	
	movl	%eax,	-4(%ebp)
	movl	%eax,	(%esp)
	movl	-8(%ebp),%ebx
	movl	%ebx,	4(%esp)
	call	input

	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)	
	movl	-8(%ebp),%eax
	movl	%eax,	4(%esp)
	call	sort
	
	###################
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)	
	movl	-8(%ebp),%eax
	movl	%eax,	4(%esp)
	call	test_sort
	cmpl	$0,	%eax
	je	unsuccess
	jmp	success
unsuccess:
	movl	$msg_p4_err,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit

success:
	movl	$msg_p3,(%esp)
	call	puts
			
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)	
	movl	-8(%ebp),%eax
	movl	%eax,	4(%esp)
	call	output
	
	movl	-4(%ebp),%eax
	movl	%eax,	(%esp)
	call	free
	movl	$0,	-4(%ebp)
	movl	$0,	(%esp)
	call	exit
	

.globl	test_sort
.type	test_sort,@function
test_sort:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	movl	$0,	-4(%ebp)
	jmp	mn_cond4
mn_for4:
	movl	-4(%ebp),%eax
	addl	$1,	%eax
	leal	0(,%eax,4),%ecx	
	movl	8(%ebp),%ebx
	addl	%ecx,	%ebx
	###################
	movl	-4(%ebp),%eax
	leal	0(,%eax,4),%ecx	
	movl	8(%ebp),%edx
	addl	%ecx,	%edx
	movl	(%edx),	%edx
	cmpl	%edx,	(%ebx)
	jg	mn_break
	
	
	addl	$1,	-4(%ebp)
mn_cond4:
	movl	12(%ebp),%eax
	subl	$1,	%eax
	cmpl	%eax,	-4(%ebp)
	jl	mn_for4
	jmp	mn_out
mn_break:
	movl	$0,	%eax
mn_out:
	movl	$1,	%eax	
	movl	%ebp,	%esp
	popl	%ebp
	ret	

.globl	output
.type   output,@function
output:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	movl	$0,	-4(%ebp)
	jmp	mn_cond3
mn_for3:
	movl	-4(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	8(%ebp),%ebx
	movl	(%ebx,%eax,4),%edx
	movl	%edx,	8(%esp)
	movl	$msg_p5,(%esp)
	call	printf
	addl	$1,	-4(%ebp)
mn_cond3:
	movl	-4(%ebp),%eax
	movl	12(%ebp),%ebx
	cmpl	%ebx,	%eax
	jl	mn_for3
	
	movl	%ebp,	%esp
	popl	%ebp
	ret


.globl	sort
.type   sort,@function
sort:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	$1,	-8(%ebp)
	jmp 	mn_cond2
mn_for2:
	movl	8(%ebp),%edx
	movl	-8(%ebp),%eax
	leal	0(,%eax,4),%ebx
	addl	%ebx,	%edx
	movl	(%edx),	%edx
	movl	%edx,	-12(%ebp)
	movl	-8(%ebp),%eax
	subl	$1,	%eax
	movl	%eax,	-4(%ebp)
	#######################whileloop###############
	jmp 	while_cond	
while_loop:
	movl	8(%ebp),%edx
	movl	-4(%ebp),%eax
	leal	0(,%eax,4),%ecx
	addl	%ecx,	%edx
	movl	(%edx),	%ecx
	######################
	movl	8(%ebp),%edx
	movl	-4(%ebp),%eax
	addl	$1, %eax
	leal	0(,%eax,4),%ebx
	addl	%ebx,	%edx
	movl	%ecx,	(%edx)
	######################
	subl	$1,	-4(%ebp)
	movl	-4(%ebp),%eax
	addl	$1,	%eax
	leal	0(,%eax,4),%ecx
	movl	8(%ebp), %edx
	addl	%edx,	%ecx
	#movl	(%ecx),	%ecx
	movl	-12(%ebp),%ebx
	movl	%ebx,	(%ecx)

while_cond:	
	cmpl	$0,	-4(%ebp)
	js	mn_add
	movl	8(%ebp),%edx
	movl	-4(%ebp),%eax
	leal	0(,%eax,4),%ebx
	addl	%ebx,	%edx
	movl	(%edx),	%edx
	cmpl	-12(%ebp),%edx
	#cmpl	%ecx,	%ebx
	jg	while_loop
mn_add:
	addl	$1,	-8(%ebp)
mn_cond2:
	movl	12(%ebp),%eax
	movl	-8(%ebp),%ebx
	cmpl	%eax,	%ebx
	jb	mn_for2
	
	movl	%ebp,	%esp
	popl	%ebp
	ret
	
.globl	input
.type	input,@function

input:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	#movl	$0,	-4(%ebp)
	
	movl	$0,	(%esp)
	call	time
	movl	%eax,	(%esp)
	call	srand
	movl	$0,	-4(%ebp)
	jmp 	mn_cond1
mn_for1:
	call	rand	
	movl	8(%ebp),%edx
	movl	-4(%ebp),%ebx
	movl	%eax, (%edx,%ebx,4)
	addl	$1,	-4(%ebp)
mn_cond1:
	movl	-4(%ebp),%eax
	movl	12(%ebp),%ebx
	cmpl	%ebx,	%eax
	jl	mn_for1	
	
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl xcalloc
.type  xcalloc,@function

xcalloc:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	movl	$0,	-4(%ebp)	
	movl	12(%ebp),%eax
	movl	%eax,	(%esp)
	movl	8(%ebp),%eax
	movl	%eax,	4(%esp)
	call 	calloc
	movl	%eax,	-4(%ebp)
	cmpl	$0,	-4(%ebp)
	je	out_err
	movl	-4(%ebp), %eax
	movl	%ebp,	%esp
	popl	%ebp
	ret	

out_err:
	movl	$msg_p6_err,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit

invalid:
	movl	$msg_p2_err,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit
	
usage:
	movl	$msg_p1_err,(%esp)
	call	puts
	movl	$-1,	(%esp)
	call	exit

		
