#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TRUE 1
#define FALSE 0

typedef int bool;

void *xcalloc( size_t, size_t );
void input( int *, size_t );
void output( int *, size_t );
bool test_sort(int *, size_t );
void sort(int *, size_t);

int main(int argc, char** argv)
{
	int *a = NULL;
	size_t n = 0;
	if (argc != 2)
	{
		puts("Usage error");
		exit(EXIT_FAILURE);
	}
	n = atoi(argv[1]);
	if (n <= 0)
	{
		puts("invalid command line argument");
		exit(EXIT_FAILURE);
	}
	a = (int*)xcalloc(n,sizeof(int));
	
	input(a, n);
	sort(a,n);
	if(test_sort(a,n) == TRUE)
	{
		puts("sort success");
	}
	else
	{
		puts("sort failure");
		exit(EXIT_FAILURE);
	}
	output(a, n);
	free(a);
	a = NULL;
	exit(EXIT_SUCCESS);
}

void input(int *a, size_t n)
{
	int i;
	srand(time(0));
	for( i = 0; i < n; i++ )
		a[i] = rand();
	return;
}

void output(int *a, size_t n)
{
	int i;
	for(i = 0; i < n; i++)
		printf("a[%d]:%d\n",i,a[i]);
	return;
}

void sort(int *a, size_t n)
{
	int i;
	int j;
	int key;
	
	for(j = 1; j < n ; j++)
	{
		key = a[j];
		i = j - 1;
		while (i >= 0 && a[i] > key)
		{
			a[i + 1] = a[i];
			i = i - 1;
			a[i + 1] = key;
		}
	}
	return;
}

bool test_sort(int *a, size_t n)
{
	int i;
	for (i = 0; i < n -1; i++)
		if(a[i] > a[i + 1])
			return FALSE;
	return (TRUE);
}

void *xcalloc(size_t i, size_t s)
{
	void *ptr = NULL;
	ptr = calloc( i, s);
	if(ptr == NULL)
	{
		puts("fatal memory");
		exit(EXIT_FAILURE);
	}
	return ptr;
}
