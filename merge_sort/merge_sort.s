
# parameter offses on stack w.r.t. esp reg while calling any function
.equ p1, 0
.equ p2, 4
.equ p3, 8
.equ p4, 12
.equ p5, 16

# local storage offsets on stack w.r.t. ebp reg while accessing local variables within function
.equ loc1, -4
.equ loc2, -8
.equ loc3, -12
.equ loc4, -16
.equ loc5, -20
.equ loc6, -24
.equ loc7, -28
.equ loc8, -32
.equ loc9, -36

# arguments received to functions, their offsets w.r.t. ebp reg 
.equ arg1, 8
.equ arg2, 12
.equ arg3, 16
.equ arg4, 20 

# constants
.equ TRUE, 1
.equ FALSE, 0

.equ EXIT_SUCCESS, 0
.equ EXIT_FAILURE, -1

.equ CAP, 1000000	# <= 1M
.section .rodata
	msg_p1:
	.string "%s<total count of numbers to be sort>\n"
	msg_p2:
	.string "memory can not be allocated\n"
	msg_p3:
	.string "arr[%d] = %d\n"
	msg_p4:
	.string "merge sort failed\n"
	msg_p5:
	.string "merge sort success\n"
.section .text
	.globl 	main
	.type		main@function

main:
	pushl	%ebp
	movl	%esp,	%ebp
	andl	$-16,	%esp
	subl	$16,	%esp

	movl	4(%ebp),%edx
	cmpl	$2,	%edx
	jne	if_err_exit	
	movl	12(%ebp),%eax
	movl	%eax,	(%esp)
	call	atoi
	movl	%eax,	-4(%ebp)
	movl	-4(%ebp),%ecx
	movl	%ecx,	(%esp)
	movl	$4,	4(%esp)
	call	xcalloc	

	movl	%eax,	-8(%ebp)
	movl	-8(%ebp),%ecx
	movl	-4(%ebp),%edx
	movl	%ecx,	(%esp)
	movl	%edx,	4(%esp)
	call	input

	movl	-8(%ebp),%ecx
	movl	-4(%ebp),%edx
	movl	%ecx,	(%esp)
	movl	%edx,	4(%esp)
	call	sort	

	movl	-8(%ebp),%ecx
	movl	-4(%ebp),%edx
	movl	%ecx,	(%esp)
	movl	%edx,	4(%esp)
	call	output

	movl	-8(%ebp),%ecx
	movl	-4(%ebp),%edx
	movl	%ecx,	(%esp)
	movl	%edx,	4(%esp)
	call	test_sort

	movl	-8(%ebp),%ecx
	movl	%ecx,	(%esp)
	call 	xfree	
	movl	$0,	(%esp)
	call	exit
if_err_exit:
	movl	8(%ebp),%edx
	movl	%edx,	4(%esp)
	movl	$msg_p1,(%esp)
	call	printf
	movl	$-1,	(%esp)
	call 	exit

.globl	test_sort
.type		test_sort@function
test_sort:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	$0,	-4(%ebp)
	jmp	mn_test_sort_cond
mn_test_sort_for:

	movl	-4(%ebp),	%eax
	movl	8(%ebp),	%ebx
	movl	(%ebx,%eax,4),%eax
	movl	-4(%ebp),%ebx
	addl	$1,	%ebx
	movl	8(%ebp),%ecx
	movl	(%ecx,%ebx,4),%ebx
	cmpl	%eax,	%ebx
	jl	out_test_sort	
	addl	$1,	-4(%ebp)
mn_test_sort_cond:
	movl	12(%ebp),%eax
	subl	$1,	%eax
	cmpl	%eax,-4(%ebp)
	jl	mn_test_sort_for	
	jmp	success_out
out_test_sort:
	movl	$msg_p4,(%esp)
	call	printf
	jmp	out_test
success_out:
	movl	$msg_p5,(%esp)
	call	printf
out_test:
	movl	%ebp,	%esp
	popl	%ebp
	ret



.globl	output
.type		output@function
output:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$32,	%esp
	
	movl	$0,	-4(%ebp)
	jmp	mn_output_cond
mn_output_for:
	movl	$msg_p3,(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,	4(%esp)
	movl	8(%ebp),%ebx
	movl	(%ebx,%eax,4),%ecx
	movl	%ecx,	8(%esp)
	call	printf		
	addl	$1,	-4(%ebp)
mn_output_cond:
	movl	12(%ebp),%eax
	cmpl	%eax,-4(%ebp)
	jl	mn_output_for

	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	sort
.type		sort@function
sort:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	8(%ebp),%eax
	movl	12(%ebp),%ebx
	subl	$1,	%ebx
	movl	%eax,	(%esp)
	movl	$0,	4(%esp)
	movl	%ebx,	8(%esp)
	call	merge_sort

	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	merge_sort
.type		merge_sort@function
merge_sort:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$32,	%esp

	movl	16(%ebp),%eax
	subl	12(%ebp),%eax
	movl	$2,	%ebx
	xorl	%edx,	%edx
	divl	%ebx
	addl	12(%ebp),%eax
	movl	%eax,	-4(%ebp)
	
	movl	12(%ebp),%eax
	cmpl	16(%ebp),%eax
	jge	out_merge_sort
	movl	8(%ebp),%eax
	movl	12(%ebp),%ebx
	movl	-4(%ebp),%ecx
	movl	%eax,	(%esp)
	movl	%ebx,	4(%esp)
	movl	%ecx,	8(%esp)
	call	merge_sort

	movl	-4(%ebp),%eax
	addl	$1,	%eax
	movl	8(%ebp),%ebx
	movl	16(%ebp),%ecx
	movl	%ebx,	(%esp)
	movl	%eax,	4(%esp)
	movl	%ecx,	8(%esp)
	call	merge_sort
	
	movl	8(%ebp),%eax
	movl	12(%ebp),%ebx
	movl	-4(%ebp),%ecx
	movl	16(%ebp),%edx
	movl	%eax,	(%esp)
	movl	%ebx,	4(%esp)
	movl	%ecx,	8(%esp)
	movl	%edx,	12(%esp)
	call	merge
			
out_merge_sort:
	movl	%ebp,	%esp
	popl	%ebp
	ret

#void merge(int a[],int p, int q, int r)
.globl	merge
.type		merge@function
merge:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$64,	%esp
	movl	$0,	-4(%ebp)
	movl	$0,	-8(%ebp)
	movl	$0,	-12(%ebp)	
	#from_a1 = from_a2 = 0(false)
	movl	$0,	-16(%ebp)
	movl	$0,	-20(%ebp)
	#n1 = q - p + 1	
	movl	12(%ebp),%eax
	movl	16(%ebp),%ebx
	subl	%eax,	%ebx
	addl	$1,	%ebx
	movl	%ebx,	-24(%ebp)#n1
	
	#int *a1 = (int*)xcalloc(n1,sizeof(int));
	movl	%ebx,	(%esp)
	movl	$4,	4(%esp)
	call	xcalloc
	movl	%eax,	-28(%ebp)#a1
	
	#n2 = r - q
	movl	20(%ebp),%ebx
	movl	16(%ebp),%ecx
	subl	%ecx,	%ebx
	movl	%ebx,	-32(%ebp)#n2
	
	#int *a2 = (int*)xcalloc(n2,sizeof(int));
	movl	%ebx,	(%esp)
	movl	$4,	4(%esp)
	call	xcalloc
	movl	%eax,	-36(%ebp)#a2
	
	
	#for(i = 0; i < n1; i++)
	movl	$0,	-4(%ebp)
	jmp	mn_merge_cond1
mn_merge_for1:
	#a[p+i]
	movl	12(%ebp),%eax
	addl	%ecx,	%eax
	movl	8(%ebp),%ebx
	movl	(%ebx,%eax,4),%edx
	#a1[i]
	movl	-28(%ebp),%eax
	#a1[i] = a[p+1]
	movl	%edx,	(%eax,%ecx,4)
	addl	$1,	-4(%ebp)
mn_merge_cond1:
	movl	-4(%ebp),	%ecx
	cmpl	-24(%ebp),	%ecx
	jl	mn_merge_for1
	
	#for(i = 0; i < n2; i++)
	movl	$0,	-4(%ebp)
	jmp	mn_merge_cond2
mn_merge_for2:
	#a[q+i+1]
	movl	16(%ebp),%eax
	addl	%ecx,	%eax
	addl	$1,	%eax
	movl	8(%ebp),%ebx
	movl	(%ebx,%eax,4),%edx
	#a2[i] = a[q+i+1]
	movl	-36(%ebp),	%eax
	movl	%edx,	(%eax,%ecx,4)
	addl	$1,	-4(%ebp)
mn_merge_cond2:
	movl	-4(%ebp),	%ecx
	cmpl	-32(%ebp),%ecx
	jl	mn_merge_for2
	
	#k = p; //index into orignal array, a[] , (p <= k < n)
	movl	12(%ebp),%eax	
	movl	%eax,	-12(%ebp)
	#i = 0; //index into left array, a1[] , (0 <= i < n1)
	movl	$0,	-4(%ebp)
	#j = 0; // index into right array, a2 , (0 <= j < n2)
	movl	$0,	-8(%ebp)
	
	#while(true)
	jmp	while_cond1
first_if:
#	if(i == n1)
#	{
#		from_a1 = TRUE;
#		break;
#	}
	movl	-24(%ebp),%eax
	cmpl	%eax,	-4(%ebp)
	jne	second_if
	movl	$1,	-16(%ebp)
	jmp	while_break
second_if:
#	if(j == n2)
#	{
#		from_a2 = TRUE;
#		break;
#	}
	movl	-32(%ebp),%eax
	cmpl	%eax,	-8(%ebp)
	jne	third_if
	movl	$1,	-20(%ebp)
	jmp	while_break
third_if:		
#	if(a1[i] <= a2[j])
#	{
#		a[k] = a1[i];
#		i = i + 1;
#	}
	movl	-36(%ebp),%eax
	movl	-8(%ebp),%ebx	
	movl	(%eax,%ebx,4),%ecx	
	
	movl	-28(%ebp),%eax
	movl	-4(%ebp),%ebx
	movl	(%eax,%ebx,4),%edx

	cmpl	%ecx,	%edx	
	jg	else
	movl	8(%ebp),%eax
	movl	-12(%ebp),%ebx
	movl	%edx,	(%eax,%ebx,4)
	addl	$1,	-4(%ebp)
	jmp	out_else
else:	
#	else
#	{
#		a[k] = a2[j];
#		j = j + 1;
#	}
	movl	8(%ebp),%eax
	movl	-12(%ebp),%ebx
	movl	%ecx,	(%eax,%ebx,4)
	addl	$1,	-8(%ebp)
out_else:
	addl	$1,	-12(%ebp)
while_cond1:
	jmp	first_if
while_break:
fourth_if:
#	if(from_a1 == TRUE)
#	{//a1[] exhausted, copy remaining already sorted item from a2[]
#		while(j < n2)
#		{
#			a[k] = a2[j];
#			k = k + 1;
#			j = j + 1;
#		}
#	}
	cmpl	$1,	-16(%ebp)
	jne	fift_if
	jmp	while_cond2
while_loop1:
	movl	-36(%ebp),%eax
	movl	-8(%ebp),%ebx
	movl	(%eax,%ebx,4),%ecx
	movl	8(%ebp),%eax
	movl	-12(%ebp),%ebx
	movl	%ecx,	(%eax,%ebx,4)
	addl	$1,	-12(%ebp)
	addl	$1,	-8(%ebp)
while_cond2:
	movl	-32(%ebp),%eax
	cmpl	%eax,	-8(%ebp)
	jl	while_loop1	
fift_if:
#	if(from_a2 == TRUE)
#	{//a2[] exhausted, copy remaining already sorted item from a1[]
#		while(i < n1)
#		{
#			a[k] = a1[i];
#			i = i + 1;
#			k = k + 1;
#		}
#	}
	cmpl	$1,	-20(%ebp)
	jne	out
	jmp	while_cond3
while_loop2:
	movl	-28(%ebp),%eax
	movl	-4(%ebp), %ebx
	movl	(%eax,%ebx,4),%ecx
	movl	8(%ebp),%eax
	movl	-12(%ebp),%ebx
	movl	%ecx,	(%eax,%ebx,4)
	addl	$1,	-4(%ebp)
	addl	$1,	-12(%ebp)
while_cond3:
	movl	-24(%ebp),%ebx
	cmpl	%ebx,	-4(%ebp)
	jl	while_loop2

out:
	movl	-28(%ebp),%eax
	movl	%eax,	(%esp)
	call	xfree
	movl	-36(%ebp),%ebx
	movl	%ebx,	(%esp)
	call	xfree
	
	movl	%ebp,	%esp
	popl	%ebp
	ret	

	
.globl	xfree
.type		xfree@function
xfree:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	movl	8(%ebp),%ebx
	cmpl	$0,	%ebx
	jle	out_free
	movl	8(%ebp),%ebx
	movl	%ebx,	(%esp)
	call	free
	movl	$0,	8(%ebp)
out_free:	
	movl	%ebp,	%esp
	popl	%ebp
	ret
	

.globl	input
.type		input@function
input:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp
	
	movl	$0,	(%esp)
	call	time
	movl	%eax,	(%esp)
	call	srand
	
	movl	$0,	-4(%ebp)
	jmp	mn_cond1
mn_for1:
	call	rand
	xorl	%edx,	%edx
	movl	$100000,	%ecx
	divl	%ecx
	
	movl	8(%ebp),%ebx
	movl	-4(%ebp),%ecx
	movl	%edx,	(%ebx,%ecx,4)
	addl	$1,	-4(%ebp)		
mn_cond1:
	movl	12(%ebp),%ecx
	cmpl	%ecx,	-4(%ebp)
	jl	mn_for1			
	
	movl	%ebp,	%esp
	popl	%ebp
	ret

.globl	xcalloc
.type		xcalloc@function
xcalloc:
	pushl	%ebp
	movl	%esp,	%ebp
	subl	$16,	%esp		
	
	movl	8(%ebp),%ecx
	movl	12(%ebp),%edx
	movl	%ecx,	(%esp)
	movl	%edx,	4(%esp)
	call	calloc
	cmpl	$0,	%eax
	je	ret_err
	movl	%ebp,	%esp
	popl	%ebp
	ret
ret_err:
	movl	$msg_p2,(%esp)
	call	printf	
	movl	$0,	(%esp)
	call	exit
	
